(ns static-site-generator.core
  (:require [selmer.parser :refer :all]
            [selmer.util :refer :all]
            [ring.middleware.defaults :refer [wrap-defaults]]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [static-site-generator.file-processing :refer [clean]]
            [static-site-generator.assets :refer [render-assets move-robots]]
            [static-site-generator.changelog :refer [render-changelog]]
            [static-site-generator.fifty-two-weeks :refer [render-fifty-two-weeks]]
            [static-site-generator.now :refer [render-now]]
            [static-site-generator.resume :refer [render-resume]]
            [static-site-generator.personal :refer [render-personal]]
            [static-site-generator.book-notes :refer [render-book-notes]]
            [static-site-generator.generate-rss-feeds :refer [generate-rss-feeds]])
  (:gen-class))

;; Cache the time the build is run
(def build-time (System/currentTimeMillis))

(selmer.parser/cache-off!)
(selmer.parser/set-resource-path! "_site")

;; When this filter is used return the build timestamp
(add-filter! :build-timestamp (fn [_] build-time))

(defn render-home []
  (spit "resources/public/index.html" (selmer.parser/render-file "_content/home.html" {})))

(def handler
  (routes
   (GET "/foo" [] "Hello Foo")
   (GET "/bar" [] "Hello Bar")))

(def app
  (-> handler
      (wrap-defaults {:static {:files "resources/public"}})))

(defn -main
  "Builds the static site"
  []
  (println "~~~ Cleaning")
  (clean)
  (println "~~~ Rendering Home")
  (render-home)
  (println "~~~ Rendering Now")
  (render-now)
  (println "~~~ Rendering Resume")
  (render-resume)
  (println "~~~ Rendering Personal")
  (render-personal)
  (println "~~~ Rendering Assets")
  (render-assets)
  (println "~~~ Rendering Changelog")
  (render-changelog)
  ; (println "~~~ Rendering 52 Weeks")
  ; (render-fifty-two-weeks)
  ; (println "~~~ Rendering Book Notes")
  ; (render-book-notes)
  (println "~~~ Rendering RSS feeds based on build output")
  ;; Must be last since it requires built dir structure
  (generate-rss-feeds)
  (println "~~~ Moving robots.txt to public")
  (move-robots)
  (println "Done!"))
