(ns static-site-generator.fifty-two-weeks
  (:require [static-site-generator.file-processing :refer [process-html
                                                           process-markdown
                                                           get-directory-listing
                                                           new-public-dir
                                                           get-root-path-for-url]]
            [selmer.parser :refer :all]
            [clojure.string :as str]
            [markdown.core :refer :all]))


(defn process-years
  "Gets year folders and processes index.html in each"
  [weeks-by-year root-path]
  (let [html-root (get-root-path-for-url ".html" ((str/split root-path #"/52-weeks") 1))
        year (first (re-seq #"\d{4}" root-path))
        ;; str/split to remove _site is a hack. selmer resource root set in core.clj
        html (process-html ((str/split root-path #"_site/") 1) {:children (reverse (sort-by :created_date (weeks-by-year year))) :page_name :52-weeks})
        new-path (str "public/52-weeks/" html-root)]
    (new-public-dir new-path)
    (spit (str "resources/" new-path "/index.html") html)
    {:name html-root :year year}))

(defn process-weeks
  "Process each week's article.md file within a year"
  [week-path]
  (let [md-path (get-root-path-for-url ".md" ((str/split week-path #"/52-weeks") 1))
        md (process-markdown week-path)
        new-path (str "public/52-weeks/" md-path)
        year (first (re-seq #"\d{4}" week-path))]
    (new-public-dir new-path)
    (spit (str "resources/" new-path "/index.html") (md :template))
    (assoc (md :metadata) :name md-path :year year)))

(defn render-fifty-two-weeks []
  (new-public-dir "public/52-weeks")
  (let [sub-dirs (get-directory-listing "resources/_site/_content/52-weeks")
        weeks-paths (filter #(re-find #"\.md$" %) sub-dirs)
        year-paths (filter #(re-find #"/\d{4}\/[a-z]*.html$" %) sub-dirs)
        weeks (map process-weeks weeks-paths)
        weeks-by-year (group-by :year weeks)
        years (map (partial process-years weeks-by-year) year-paths)]
        ;; children (map process-children articles)]
    (spit "resources/public/52-weeks/index.html" (selmer.parser/render-file "_content/52-weeks/index.html" {:children (reverse (sort-by :name years)) :page_name :52-weeks}))))