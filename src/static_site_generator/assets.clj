(ns static-site-generator.assets
  (:require
   [me.raynes.fs :as fs]))

(defn render-assets []
  (fs/copy-dir "resources/_site/assets" "resources/public/"))

(defn move-robots []
  (fs/delete "resources/public/assets/robots.txt")
  (fs/copy "resources/_site/assets/robots.txt" "resources/public/robots.txt"))
