(ns static-site-generator.file-processing
  (:require [selmer.parser :refer :all]
            [selmer.util :refer :all]
            [markdown.core :refer :all]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [net.cgrand.enlive-html :as enlive])
  (:import (java.io StringReader StringWriter))
  (:gen-class))

(selmer.parser/cache-off!)

(defn process-markdown [file-path]
  (let [input     (new StringReader (slurp file-path))
        output    (new StringWriter)
        metadata  (md-to-html input output :parse-meta? true :footnotes? true :reference-links? true)
        html      (.toString output)
        word-count     (->> html
                            enlive/html-snippet
                            enlive/texts
                            (mapcat #(re-seq #"\s" %))
                            count)
        template-path (str "_templates/" (metadata :layout) ".html")
        template-vars (merge {:body html
                              :created_date (metadata :created_date)
                              :page_title (metadata :title)
                              :short_title (metadata :short_title)
                              :page_image (metadata :image_url)
                              :page_description (metadata :description)
                              :year (metadata :year)
                              :word_count word-count}
                             metadata)
        templated (selmer.parser/render-file template-path template-vars)]
    {:template templated :metadata metadata :html html}))

(defn process-html [file-path template-vars]
  (selmer.parser/render-file file-path template-vars))

(defn get-directory-listing [dir-path]
  (map #(.getPath %) (file-seq (io/file dir-path))))

(defn get-directory-listing-seq [dir-path]
  (for [x (file-seq (io/file dir-path))]
    (keep not-empty (str/split (.getPath x) #"/"))))

(defn list-middle [path-list]
  (rest (drop-last path-list)))

(defn get-root-path-for-url [file-extensions path]
  (when (str/includes? path file-extensions)
    (->> (str/split path #"/")
         (list-middle)
         (str/join "/"))))

(defn delete-directory-recursive
  "Recursively delete a directory."
  [^java.io.File file]
  ;; when `file` is a directory, list its entries and call this
  ;; function with each entry. can't `recur` here as it's not a tail
  ;; position, sadly. could cause a stack overflow for many entries?
  ;; thanks to @nikolavojicic for the idea to use `run!` instead of
  ;; `doseq` :)
  (when (.isDirectory file)
    (run! delete-directory-recursive (.listFiles file)))
  ;; delete the file or directory. if it it's a file, it's easily
  ;; deletable. if it's a directory, we already have deleted all its
  ;; contents with the code above (remember?)
  (io/delete-file file))

(defn clean []
  (delete-directory-recursive (io/file "resources/public"))
  (.mkdirs (java.io.File. "resources/public")))

(defn new-public-dir [dir-struct]
  (.mkdirs (java.io.File. (str "resources/" dir-struct))))
