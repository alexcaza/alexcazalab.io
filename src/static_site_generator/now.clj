(ns static-site-generator.now
  (:require [static-site-generator.file-processing :refer [new-public-dir]]
            [selmer.parser :refer :all]))



(defn render-now []
  (new-public-dir "public/now")
  (spit "resources/public/now/index.html" (selmer.parser/render-file "_content/now/now.html" {:page_name :now})))