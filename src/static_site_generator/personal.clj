(ns static-site-generator.personal
  (:require [static-site-generator.file-processing :refer [process-html
                                                           process-markdown
                                                           get-directory-listing
                                                           new-public-dir
                                                           get-root-path-for-url]]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.instant :as ins]
            [markdown.core :refer :all]))

(defn process-children [article-path]
  (let [root-path (get-root-path-for-url ".md" ((str/split article-path #"/personal") 1))
        md (process-markdown article-path)
        new-path (str "public/personal/" root-path)]
    (new-public-dir new-path)
    (spit (str "resources/" new-path "/index.html") (md :template))
    (assoc (md :metadata) :name root-path)))

(defn render-personal []
  (new-public-dir "public/personal")
  (let [sub-dirs (get-directory-listing "resources/_site/_content/personal")
        articles (filter #(re-find #"\.md$" %) sub-dirs)
        children (map process-children articles)
        sorted-children (sort-by :created_date #(compare (ins/read-instant-date %2) (ins/read-instant-date %1)) children)]
    (spit "resources/public/personal/index.html" (selmer.parser/render-file "_content/personal/index.html" {:children sorted-children :page_name :thoughts}))))
