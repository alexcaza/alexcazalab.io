(ns static-site-generator.generate-rss-feeds
  (:require [static-site-generator.file-processing :refer [get-directory-listing
                                                           get-root-path-for-url process-markdown]]
            [clojure.string :as str]
            [clj-rss.core :as rss]
            [clojure.instant :refer [read-instant-date]])
  (:import (java.time Instant LocalDate)))

(defn rss-channel-root [title root-path description]
  {:title title
   :feed-url (str "https://alexcaza.com/" root-path "/feed.xml")
   :description description
   :link (str "https://alexcaza.com/" root-path)
   :lastBuildDate (Instant/now)})

(defn process-md [root-path title-fn week-path]
  (let [article-path (get-root-path-for-url ".md" (last (str/split week-path (re-pattern (str "/" root-path)))))
        new-path (str "https://alexcaza.com/" root-path "/" article-path)
        md (process-markdown week-path)
        metadata (md :metadata)
        parsed (.toString (.atStartOfDay (LocalDate/parse (metadata :created_date))))
        instant (.toInstant (read-instant-date parsed))]
    {:title (title-fn metadata)
     :link new-path
     :description (metadata :description)
     :pubDate instant}))

(defn generate-fifty-two-weeks-feed []
  (let [sub-dirs (get-directory-listing "resources/_site/_content/52-weeks")
        week-paths (filter #(re-find #"\.md$" %) sub-dirs)
        rss-items (->> week-paths
                       (map (partial process-md "52-weeks" #(str (% :year) " | " (% :title))))
                       (sort-by :pubDate)
                       (reverse))]
    (spit
     "resources/public/52-weeks/feed.xml"
     (rss/channel-xml
      (rss-channel-root
       "52 Weeks of Creation | Alex Caza"
       "52-weeks"
       "Each week, I'm going to create something new. This is an RSS feed of each of those creations")
      rss-items))))


(defn generate-thoughts-feed []
  (let [sub-dirs (get-directory-listing "resources/_site/_content/personal")
        week-paths (filter #(re-find #"\.md$" %) sub-dirs)
        rss-items (->> week-paths
                       (map (partial process-md "personal" #(% :title)))
                       (sort-by :pubDate)
                       (reverse))]
    (spit
     "resources/public/personal/feed.xml"
     (rss/channel-xml
      (rss-channel-root
       "Thoughts & Essays | Alex Caza"
       "personal"
       "A collection of my thoughts and short essays")
      rss-items))))

(defn generate-book-notes-feed []
  (let [sub-dirs (get-directory-listing "resources/_site/_content/book-notes")
        week-paths (filter #(re-find #"\.md$" %) sub-dirs)
        rss-items (->> week-paths
                       (map (partial process-md "book-notes" #(str (% :book_title) " by " (str/join ", " (% :book_authors)))))
                       (sort-by :pubDate)
                       (reverse))]
    (spit
     "resources/public/book-notes/feed.xml"
     (rss/channel-xml
      (rss-channel-root
       "Book Notes | Alex Caza"
       "book-notes"
       "A collections of notes for books that I've read and enjoyed. Most of the books I spend time organizing notes for provided immense value to me, so consider them all high priority reads for me.")
      rss-items))))

(defn generate-rss-feeds []
  ; (generate-fifty-two-weeks-feed)
  (generate-thoughts-feed))
  ; (generate-book-notes-feed))
