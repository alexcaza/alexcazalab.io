(ns static-site-generator.changelog
  (:require [static-site-generator.file-processing :refer [new-public-dir]]
            [selmer.parser :refer :all]))

(defn render-changelog []
  (new-public-dir "public/changelog")
  (spit "resources/public/changelog/index.html" (selmer.parser/render-file "_content/changelog/changelog.html" {})))