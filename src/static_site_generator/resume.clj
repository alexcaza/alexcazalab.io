(ns static-site-generator.resume) (ns static-site-generator.resume
                                    (:require [static-site-generator.file-processing :refer [new-public-dir]]
                                              [selmer.parser :refer :all]))



(defn render-resume []
  (new-public-dir "public/resume")
  (spit "resources/public/resume/index.html" (selmer.parser/render-file "_content/resume/resume.html" {:page_name :resume})))