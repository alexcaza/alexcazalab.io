(ns static-site-generator.book-notes
  (:require [static-site-generator.file-processing :refer [process-html
                                                           process-markdown
                                                           get-directory-listing
                                                           new-public-dir
                                                           get-root-path-for-url]]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.instant :as ins]
            [markdown.core :refer :all]))

(defn process-children [article-path]
  (let [root-path (get-root-path-for-url ".md" ((str/split article-path #"/book-notes") 1))
        md (process-markdown article-path)
        new-path (str "public/book-notes/" root-path)]
    (new-public-dir new-path)
    (spit (str "resources/" new-path "/index.html") (md :template))
    (assoc (md :metadata) :name root-path)))

(defn render-book-notes []
  (new-public-dir "public/book-notes")
  (let [sub-dirs (get-directory-listing "resources/_site/_content/book-notes")
        articles (filter #(re-find #"\.md$" %) sub-dirs)
        children (map process-children articles)
        sorted-children (sort-by :date_read #(compare (ins/read-instant-date %2) (ins/read-instant-date %1)) children)]
    (spit "resources/public/book-notes/index.html" (selmer.parser/render-file "_content/book-notes/index.html" {:children sorted-children :page_name :book-notes}))))
