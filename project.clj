(defproject static-site-generator "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [selmer "1.12.50"]
                 [markdown-clj "1.11.0"]
                 [ring "1.9.5"]
                 [ring/ring-defaults "0.3.3"]
                 [compojure "1.6.2"]
                 [clj-commons/fs "1.6.307"]
                 [org.clojure/data.json "2.4.0"]
                 [clj-rss "0.4.0"]
                 [enlive "1.1.6"]]
  :main static-site-generator.core
  :ring {:handler static-site-generator.core/app}
  :plugins [[lein-ring "0.12.6"] [lein-auto "0.1.3"]]
  :auto {:default {:file-pattern #"\.(clj|css|html|md|edn|)$" :paths ["src/" "resources/_site"]}}
  :repl-options {:init-ns static-site-generator.core})
