# My lightweight personal website

This is the source code for my personal website. It's statically generated, and not very dynamic.

The goal of this website is to be fun to maintain, long-lasting, and very quick to load. I want this to be a place to express what I'm thinking, and not get in my way when doing so.

It's far from beautiful, but it works, and I plan on making changes to the code structure to make it a little more generic as I go.

Originally this was written in PHP, but my love of Clojure has been growing, and I thought my website would be the perfect playground for practicing with Clojure. This transition was done as part of a 52 weeks of creation series I did, which you can read about on my website by following this link: [https://alexcaza.com/52-weeks/2022/w15](https://alexcaza.com/52-weeks/2022/w15)
