---
title: "Week 28 | Crystallizing Meditation For Myself"
short_title: "Week 28"
created_date: "2022-07-16"
description: "I've been doing more personal meta-analysis when it comes to how I think, and I wanted to share what I've discovered about myself. Mostly in hopes that it might help someone else out."
layout: "_52-weeks"
year: "2022"
---

For the first time in my life, I've done something consistently for over a year. 473 days to be exact. That thing is mindfulness meditation.

Every day for the past year in a quarter, I've meditated first thing in the morning. I typically meditate for 15 minutes before starting my day. What's been wild to me is how it's taken me this long to finally feel like I'm understanding what it means to meditate, and what the practice actually is.

Meditation is a funny paradox. It's a skill that can be practiced, but not improved. And it's a state of mind that can be cultivated but not chased. In fact, chasing it and trying to improve meditation will make it harder to practice.

The last 3 days, I've had a bit of an awakening when it came to my practice. After speaking with my therapist, he opened my eyes to the fact that I still haven't fully embraced the act of meditating. The way I described my practice he noted that I've been trying to push things away as they come up, instead of trying to work with them. With that in mind, I've been approaching meditation with a new lens, and I wanted to write about what that experience has been like. Mostly has a way to help myself internalize these epiphanies.

You can read the essay here: [https://alexcaza.com/personal/what-is-meditation](/personal/what-is-meditation)
