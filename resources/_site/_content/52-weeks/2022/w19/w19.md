---
title: "Week 19 | Summer Salad Recipe"
short_title: "Week 19"
created_date: "2022-05-11"
description: "I wanted to switch it up this week and offer up a salad I made while on vacation."
layout: "_52-weeks"
year: "2022"
---

In an effort to be a more balanced individual, I took the week off of work. My girlfriend and I thought it would be nice to get out of the city a bit and head to our family cottage for the week. It's been incredibly nice to get away and disconnect a bit. Obviously not fully, seeing as I'm writing this post. However, I've done quite a bit of reading and lot of cooking to make up for it.

I despited the act of making food for a long time. After a full day of work you expect me to nourish myself? I never felt like I had the energy, or the skills. I'd much rather pay the convenience tax and get something delivered or go out to eat instead. This attitude changed during the pandemic. In part due to the pandemic restricting restaurant eating, and the other part due to founder life & salary limiting the amount of money I could spend on take-out. It put me in a position to try and make cooking an activity I enjoyed, with the output being something I could tolerate—hell maybe even love.

It started with finding some channels on YouTube that made cooking feel a lot more accessible. The further I got down the rabbit hole of cooking, the more I realized it was a creative and pragmatic discipline. Much like coffee, understanding the variables at play opens up the world to you. After building a mental model of variables at your disposal, it's about discovery and building a mental flavour bank. That's when the creative wheels can start clinking.

Calling me a master chef would be a massive overstatement. Though my skills have definitely improved over the last 2 years. I'm finally at a place where I can cook something 6/10 times during the week and it taste pretty solid. When I give myself the time and focus to really prepare something, I can generally make something that's an 8/10 on my personal scale.

Thankfully, this last week at the cottage has definitely been one of those banger weeks. Because of that, I wanted to share a recipe I made for this week's creation. It's a delicious, balanced, vegetarian summer salad. It's been a perfect, cool, meal as we've been hitting 26ºc during the days we've been up here.

If you end up making it, I'd love some feedback! Both on how the recipe was written out, but also any substitutions or feedback you might have!

## Recipe

![Summer Salad Photo](/assets/52-weeks/2022/w19/summer-salad.jpg)
Quick, balanced and slightly sweet summer salad. Will be excellent towards the end of summer when strawberries are in season.

### Ingredients

#### Salad Base

- 2 Cups fresh baby spinach (pre-washed if possible)
- 2 14oz cans green lentils - drained and rinsed
- 1/2 Pint cherry tomatoes - quartered
- 2 Medium red bell peppers - medium dice
- 1 Pint strawberries - quartered
- 2 Avocados - small diced
- 1 Block feta - small dice

#### Dressing

- 1/2 Cup olive oil
- 1/4 Cup honey
- 1/2 Cup apple cider vinegar
- Salt and pepper to taste

#### Equipment

- 1 Colander (holes small enough to prevent lentils from slipping through)
- 1 Extra large mixing bowl
- 1 Small or medium bowl to mix dressing
- 1 Medium whisk
- 1 Pair salad tongs

### Directions

#### Salad Base

Drain and rinse both cans of lentils. Add to large mixing bowl.

Load up 2 cups of spinach. Wash and dry if necessary. Add to large bowl with lentils. _If the leaves are cumbersome to bite, roughly chop them before adding._

Rinse and pat dry 1/2 pint of cherry tomatoes, bell peppers and strawberries.

Cut stems off strawberries.

Quarter cherry tomatoes and strawberries; add to large mixing bowl with the rest of the ingredients.

Medium dice bell peppers, and small dice block of feta. Add to large mixing bowl with the rest of the ingredients.

Halve avocados, pit them, then scoop out the innards on to the cutting board. Small dice each of the halves and add to the large mixing bowl with the rest of the ingredients.

#### Dressing

Measure and add the olive oil, honey and apple cider vinegar into a small or medium mixing bowl. Whisk to combine.

Taste for seasoning and add salt and pepper as desired. You can play with the ratio of the 3 ingredients to increase acidity, sweetness or fattiness depending on what you'd like to highlight.
