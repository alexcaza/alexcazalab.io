---
title: "Week 17 | On Writing Well"
short_title: "Week 17"
created_date: "2022-04-27"
description: "A collection of my notes and takeaways from On Writing Well by William Zinsser"
layout: "_52-weeks"
year: "2022"
---

I've been having some health issues these past two weeks, so I haven't really had the mental or physical energy to do much creative work. I've mostly been focused on rest and recovery.

Part of this recovery has been reading some great books—I'm currently devouring [The Year of Living Biblically](https://amzn.to/3MEcZ6n)—and revisiting highlights of books I've previously read. The goal of revisiting older books is to build myself a commonplace book in Notion, which I touched on in my [last update](/52-weeks/2022/w16).

The latest book I revisited was On Writing Well by William Zinsser. This book has had a profound impact on the way I think about writing. Not all of the takeaways have fully sunk in yet, clearly, but like most things, it's a work in progress. It's also given me a lot more confidence to write a non-fiction book of my own. There are so many good tips, mental models and framings for writing—and life in general—that I highly recommend anyone read it.

We're all essentially writers in this era, and I think we'd all benefit from the advice in this book. Honestly, if 20% of the population cared just a bit more about their writing, I think the world would move along much more smoothly.

You can read my notes, some quotes, and my takeaways here: [alexcaza.com/book-notes/on-writing-well-william-zinsser](/book-notes/on-writing-well-william-zinsser)
