---
title: "Week 33 | An updated style for weekly creations"
short_title: "Week 33"
created_date: "2022-08-17"
description: "Another bit of a cheat week. I updated the style to the weekly creation section of my website."
layout: "_52-weeks"
year: "2022"
---

I took this week off work, and I'm trying my best to not be totally invested in technology or programming during the time off.
Every once in a while it's nice to fully disconnect and reset. It makes coming back to the work more fun. And, it gives you time to come up with new ideas. However, I do want to keep the weekly creation series going without a hitch. At least until the end of the year.

Something that bugged me about my own site was the lack of information on each week before clicking on it. When I'd want to go back and reference something, I'd have to click through my files to look at the description in the metadata. Instead of obfuscating that information for no good reason, I wanted to make it a core part of the layout. So, for this week, I did just that.

It's a very minor change, and one I'm sure you've noticed by coming to this post--unless it was a direct link or the style has changed since then--but it's a change that'll also make my life easier.
