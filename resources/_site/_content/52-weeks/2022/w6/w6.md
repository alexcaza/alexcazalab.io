---
title: "Week 6 | Experimenting with sound design"
short_title: "Week 6"
created_date: "2022-02-13"
description: "I've been missing playing around in Ableton and learning about music production. So, I made a beat."
layout: "_52-weeks"
year: "2022"
---

One of the reasons I've wanted to get into making music or playing with Digital Audio Workstations in general is because sound design has always fascinated me. Whether it's synthesizers for different songs, sound effetcs or sounds used in digital products.

The fact that sounds can communicate so much to a user about a situation that's going on is such a powerful tool in product design. The best example to me is Apple. A lot of their ecosystem is designed around sounds. Outside of general notification sounds, they do an excellent job explaining which state you're in when using AirPods.

I'm by no means Jim Reekes or Hugo Verweij but I thought I'd take a shot at making _something_ this week.

## Why this sound?

I've been doing a lot of product work at Fluent these past weeks as we gear up for a pretty dramatic overhaul of the product. There's been a lot of leading strategy, doing user interviews and observations, and just being in a product mindset vs an engineer's mindset. It's been a nice change of pace. And with that change of pace, I've been thinking about how we can surprise and delight once we create a strong foundation for the product. One of those delightful additions could be sound. There's something satisfying about "completion" sounds. Especially when you're doing something that's maybe a little less sexy or exciting—like learning new vocabulary. So, I wanted to try and make something fun, chirpy and rewarding. I'm not sure I succeeded 100% but I'm pretty proud of it for my first **"real attempt"** at sound design.

I made this sound using a free synth on my iPad called [Synth One](https://audiokitpro.com/synth/), then imported the recording of the tones into Ableton for some post-processing. I kept the processing light, mostly adding some echo, compression, EQ and automating the echo wetness to fade out. I tried adding some sub-bass to give it a fuller sound but realized that in most instances, you'll be competing with other systems sounds and possibly background noise, so I kept the frequencies in the 1k–10k range.

I uploaded the WAV to [Freesound.org](https://freesound.org) under a Creative Commons 0 license, so feel free to use this, mutate it and include it in any project—commercial or personal—with 0 worries! If you do use it, I'd love to know about it just for my own curiosity! So feel free to email me if ever you do: alex [at] alexcaza [dot] com.

[Give it a listen](https://freesound.org/people/graingiant/sounds/619692/)
