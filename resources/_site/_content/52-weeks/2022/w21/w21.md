---
title: "Week 21 | Linearity Is A Lie"
short_title: "Week 21"
created_date: "2022-05-29"
description: "Moving through live linearly has been top of mind for me lately. The craving for a linear process is something I've been noticing around me, and it reminded me how much I dislike that thinking. So, I wrote a personal essay/rant about it."
layout: "_52-weeks"
year: "2022"
---

A lot of my life has been pushing back against linear processes. Moving forward in a straight line isn't something that I've ever really enjoyed or understood. Outside of time, trying to follow linear paths never seems to end up going the way I've anticipated, or it's led to destinations that were far from the original goal.

Recently, I've been caught thinking in a linear way at Fluent, and it has caused me large amounts of stress and anxiety. Which is understandable. When things stray from the _"true path,"_ it's easy to feel like you're losing control. Humans have been conditioned to think linearly, so it makes sense that when something pushes us outside of that zone of comfort, we get scared.

My childhood wasn't very linear. Hell, most of the things I've learned or tackled in my life have been far from linear. Yet, I easily fall back into the trap of linear thinking. It's such an easy default. I wanted to stop and reflect on why that might be. From that reflection came an essay. An essay, that in a lot of ways, is a look into the way I think.

Hopefully this can help someone reframe the wya they're approaching problems in their life. If not, then at least it's a nice reminder to myself when I'm stuck looking at a problem from the beginning instead of working backwards from the end.

You can read the essay here: [https://alexcaza.com/personal/forget-linear](/personal/forget-linear/)
