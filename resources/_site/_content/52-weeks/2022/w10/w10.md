---
title: "Week 10 | Command-line tool"
short_title: "Week 10"
created_date: "2022-03-13"
description: "A simple command-line tool to generate build names using word lists. Written in Clojure."
layout: "_52-weeks"
year: "2022"
---

I originally wanted to keep working on the [habit tracker](/52-weeks/2022/w9) from last week but work has been a little intense and requiring a lot of focus from me. That often means by the end of the day, I'm totally cooked and want to do light tasks.

That means I needed something a little simpler to through together this week instead of a web application.

I got the idea for a command-line tool that generated random names in the [Ubuntu format](https://wiki.ubuntu.com/DevelopmentCodeNames) while working this week, and it seemed like it could be the perfect amount of scope for a project limited to hours, not days. The idea came to me because we're going to start user testing soon and I wanted a way for us to more easily identify which builds we're testing with which cohorts. I came across a [repository on Github](https://github.com/fnichol/names) for a command-line tool that does just that, and looking through the code, I realized it's a fairly simple thing to build, so long as you have the word lists. Seeing as I'm still learning Clojure, it felt like the perfect project to tackle.

To save some time, I lifted the adjective and noun word lists from the repository I came across—thank you fnichol for publishing your work on Github under MIT—to hit the ground running.

The implementation is pretty basic, and I'm sure Clojurists would probably scream a the way the code is written, but it works really well! If anyone with more experience has better ways of doing things, please feel free to submit PRs! I'd love to learn more from expreienced Clojure artists. Though I have experience with functional-ish Javascript, I still have to re-wire my brain to the Clojure way of thinking.

Ultimately, this project ended up teaching me _a lot_ in terms of how to structure Clojure functions, files/names-spaces and just generally working with the language. At this point, it's feeling more and more like the REPL and I are one, and I have a feeling it's going to get harder and harder to go back to working without a REPL attached to whatever work I'm doing.

You can check out the repository here: [https://github.com/graingiant/build-names](https://github.com/graingiant/build-names)

The install instructions _should work_ under macOS and Unix/Linux based systems. If you end up installing it and it doesn't work, let me know!

_P.S. This entire project was written using GitHub codespaces on an iPad Pro 11"! While it's far from a perfect dev environment, it's still very workable! It gives me hope for the world of deferred computing._
