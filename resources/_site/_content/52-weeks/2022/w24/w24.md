---
title: "Week 24 | Talking about anxiety"
short_title: "Week 24"
created_date: "2022-06-17"
description: "A look at how I've gotten better at recognizing my panic triggers, and what I've been doing to manage my anxiety."
layout: "_52-weeks"
year: "2022"
---

I wasn't sure what to do for this weeks creation, and it only hit me about an hour ago. I've still been enjoying writing, and I have a personal piece I've been working on that still needs to be edited, but I couldn't find the energy to do that this week. So, instead, I thought I would shoot from the hip, write a post, and publish it without much editing.

It's a personal update on how I've been managing my anxiety and panic attacks. I'd be surprised if it has any value for anyone other than myself, but that's fine. I want to get back to writing for me. It's an easy way for me to practice the art. And, since I've been so focused on my recovery, I've become pretty self-centered, which makes writing about myself a little easier than other topics. The words tend to flow more naturally. Which makes sense, considering I'm on my mind a little too frequently right now.

You can read the piece here: [https://alexcaza.com/personal/recognizing-my-triggers](/personal/recognizing-my-triggers)
