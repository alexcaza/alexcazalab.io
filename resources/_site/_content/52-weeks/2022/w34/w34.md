---
title: "Week 34 | Build name generator redux"
short_title: "Week 34"
created_date: "2022-08-29"
description: "I built another CLI to generate build names. This time in it's written in Go."
layout: "_52-weeks"
year: "2022"
---

It's been a weekend of socializing and recovery. So, I didn't spend much time on any creative projects this week.

I've also come to the realization that I'm pretty sure I've been overtraining physically. Workouts have gotten really rough, and I feel overly exhausted by the time I'm done. Which is leading me to have much, much less energy throughout the week.

I'm planning on taking some longer physical recovery time, so hopefully that'll help rejuvenate me, and get me back on the creative train.

In the meantime, here's another take on my build names project from a few months ago. This time, however, it's written in Go.

Go is a language that's been on my radar since it's release years ago, but I never had a reason to pick it up. I don't really have much of a reason now, either, but I've been wanting to pick up something that's more performance oriented since I've managed to skirt dealing with memory addresses and "lower-level" code most of my career. Go seems like a fairly friendly entry as a high-level programmer.

I have to say, even though this is a simple project, the language has been fun to mess around with so far.

Here's the code: [https://github.com/alexcaza/build-names](https://github.com/alexcaza/build-names)
