---
title: "Week 31 | My favourite misses"
short_title: "Week 31"
created_date: "2022-08-07"
description: "A few of my favourite recent photography misses. Specially compositions that I still kind of like, even though I messed up the focus."
layout: "_52-weeks"
year: "2022"
---

Alright, you caught me. I'm kind of copping out this week. It was one of those weeks where other priorities were ahead of my weekly creation and I only remembered that I needed to do something at 7pm this evening.

Instead of trying to rush to come up with something, then it ultimately being even more lackluster than this, I wanted to share some of my favourite recent photography failures. These photos has been taken as part of my [work-in-progress](/52-weeks/2022/w22/) [called](/52-weeks/2022/w23/) [Benvenuti](/52-weeks/2022/w25/), which is a collection of photos taken around the neighbourhood I live in currently.

How could a failed photograph still be good, you ask? Well, in this case, they're compositions that I still ultimately like. I just completely messed up the focus on them. I could argue they're "artsy" if I wanted to, but that would just be a lie. So, I might as well own up to it! This project was also about honesty in the creative process, wasn't it?

Looking back on them, there are 2 that I really wished worked out. Thankfully one of them I _could_ retake if I wanted to. It's of a local Caribbean restaurant called [Lloydie's](https://lloydies.ca/)—which if you're in Montreal you should absolutely check out—which you'll see below.

[![A napkin dispenser on a round table within a retro-style restaurant setting](/assets/52-weeks/2022/w31/lloydies.jpg)](/assets/52-weeks/2022/w31/lloydies.jpg)

The second is I can't easily retake. Unless the universe decides I'm so karmically imbalanced that it should send me a gift by way of photography, I don't think it'll happen. Not only did I miss focus in this photo, but I messed up the exposure as well. There was an elderly gentlemen taking a nap in his car. From where I was standing, you could see his reflection in the side mirror. I thought it would make for a great photo if he was tack-sharp in the frame with the overpass and clouds just out of focus in the background.

Admittedly, I was a little nervous taking this photo. So, I wanted to frame it up and hit the shutter quickly so I could move on as if nothing happened. Y'know, the typical street photographer move. While I still like the composition, it could've been _much, much_ better had I taken my time.

[![A car parked to the right of the frame with an overpass in the background](/assets/52-weeks/2022/w31/overpass.jpg)](/assets/52-weeks/2022/w31/overpass.jpg)

The last two aren't anything special, I just enjoy the compositions. Besides the focus, the first one of the dog could've been better had the dog not be so close to the parking meter. It would've allowed a better angle.

The second photo doesn't actually have much of a focus issue. It's more so an exposure issue. The black and white film I was using doesn't have a ton of dynamic range, which caused quite a harsh fall off in the greys and blacks. Had there been more detail and latitude in the blacks, I suspect this composition would've looked a lot more painterly.

[![A dog tied to a parking meter waiting for his owner outside of a produce store](/assets/52-weeks/2022/w31/dog.jpg)](/assets/52-weeks/2022/w31/dog.jpg)

[![People standing and sitting outside of a closed restaurant](/assets/52-weeks/2022/w31/people.jpg)](/assets/52-weeks/2022/w31/people.jpg)

I might share more photography failures in the future. I think it's fun to see the frames that get passed on. It helps you learn more about what works and what doesn't, and gives you a sense of just how many photos are actually taken for a project for the amount that are included.
