---
title: "Week 22 | A Work In Progress"
short_title: "Week 22"
created_date: "2022-06-04"
description: "This week is a bit of a cop out. I haven't had the much creative energy this week, and most of what I'm excited about is a work in progress. Not wanting to come up empty handed, I figured I'd share a few photos from the project I'm putting together."
layout: "_52-weeks"
year: "2022"
---

This week is a bit of a cop out. I haven't had much creative energy lately, and most of what I'm excited about working on is a larger project in progress. Not wanting to come up empty handed this week, I figured I'd share a few photos from said project.

During the height of the pandemic, as a way to disconnect from work, I felt the need to pickup a creative hobby. Photography being a hobby of mine I've spent the most time with, I figured it would be nice to challenge myself and get back out to make photos. However, being tired of the always-on digital nature of my life, I felt it was time to reconnect with the analogue world and slow down. After falling down a film photography YouTube rabbit hole, I asked my parents and relatives if they had any old 35mm cameras laying around. Thankfully, they did, and they were gracious enough to give them to me.

Armed with an analogue camera and some black and white film, I set off on a journey to disconnect and pay attention to my surroundings again. It became my attempt at making due with that fact that COVID had trapped us within our neighbourhood. We didn't have a car at the time, and weren't comfortable renting one, either, which left us pretty confined for the first few months.

I needed to find a way to make due with the fact that we were stuck. Thankfully, photography being the escape that it is for me, I figured I would try to capture the beauty that does exist in our area. That's where the idea for the project came from. Instead of fighting the fact that we were confined, I wanted to make the best of it. For my own sanity, I needed to find and collect the beauty in the same buildings, types of people and shops we see every day. As things started to open up again, the project took a bit of a back seat. But lately, there's been something about it calling me to finish it. Maybe it's because we want to move at the end of the year, and coming back to make more photos will feel like cheating if we leave the area.

Our apartment is right on the border of Little Italy, which is a very small part of Montreal. It's contained within roughly a 1km radius, making it easy to walk end-to-end, top-to-bottom, within an afternoon. But, within that 1km radius is a surprising amount of history. History that I'm hoping I'll be able to capture and do justice.

The tentative name for the project is Benvenuti. It's a nod to the giant, welcoming, arches on Saint Laurent street. Which I'm hoping to have as the cover photo.

It's hard for me to say how big the project will be, but I am hoping it'll be long than [Memento](/52-weeks/2022/w1/), and filled with more written words to explain some of the history of this area. I'd also like for it to be printed and possibly sold within a few shops here. But, let's not get ahead of myself.

For now, I hope you enjoy 4 frames that I'm planning to include in the project.

[![A collection of photos from Benvenuti](/assets/52-weeks/2022/w22/contact-sheet.jpg)](/assets/52-weeks/2022/w22/contact-sheet.jpg)
