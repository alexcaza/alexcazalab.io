---
title: "Week 9 | Building my own habit tracker (part 1)"
short_title: "Week 9"
created_date: "2022-03-05"
description: "I currently use a habit tracker, but it doesn't offer cloud sync of any kind, and I don't want to lose my history. So, I'm making my own!"
layout: "_52-weeks"
year: "2022"
---

A few years ago I wanted to make some changes in my life. I wasn't happy with my body and mind. I was overweight or obese for most of my life and I wanted to take control of my physical health a little bit more before It got to be too late.

I had really gotten into the Tim Ferriss podcast around 2016 while I was looking to better myself, and something a few of the guests at the time at brought up was the idea of tracking your habits. To the guests, it was a way of turning the process of checking something off into a ritual. It was a way to connect more deeply with it, keep yourself accountable and visualize your progress. At the time there weren't as many habit tracking apps yet.

I stumbled across Way of Life, which seemed to offer everything I wanted. It operated on a pay-what-you-want model, didn't require an account to use, and made it _really easy_ to backup & export your data. It also wasn't a forced social experience like most of the other trackers at the time. I fell in love.

It made tracking habits like exercise, reading and meditation super simple. The reminders made sure I stayed on top of my habits, and the act of opening the app became an anchor to complete the actions. I was finally on track! I used it for most of 2017, only stopping once the routines felt so ingrained that I didn't need to track them anymore. Eventually the routine fell apart and I've been using it on-and-off ever since. I pick it back up when I need a kick in the ass to get back into the flow of things.

This past year has been rough in a multitude of ways. I _need_ a habit tracker again to keep me from slipping into self-destructive patterns. However, the way I structure my habits has changed. I move between 3 devices depending on context now, and I find myself forgetting to go back to my phone to check something off pretty frequently. Unfortunately, Way of Life doesn't offer cloud sync, so I can't keep my logbook consistent across my phone, iPad and computer(s) without doing an import/export dance every time.

Like all good hackers, I want to over-complicate my life by solving this minor inconvenience through code.

My plan is straightforward:

1. Write a data-transformer to take my WoL data and upload it to a Notion database (because Notion [manages my life now](/personal/simplicity-in-life-management/))
2. Create a client-side application that interfaces with my Notion database in a similar UI as WoL

This post is about part 1! I've written a data transformer for Way of Life that interfaces with my Notion database. And, because this 52 weeks of creation is all about sharing the work, I uploaded the code to [GitHub](https://github.com/graingiant/way-of-life-notion-importer).

If you go to the repo, you'll notice that it's written in Clojure. That's right, I'm learning Clojure. And no one can stop me!

I've wanted to learn a LISP dialect for years now, but never committed to it because the last 5 years have been focused on productivity, not exploration for me. Now that those tides have shifted, I'm totally down to commit to learning this absolutely wild looking language. And, I've got to say, after "struggling" with the syntax and flow of REPL based development for a week, it's finally starting to click! Now, this isn't a complicated project by any means. But the forced purity and side-effect-less development is a **fucking dream**. As someone who's generally preferred functional-style Javascript, I'm starting to feel right at home!
