---
book_title: "On Writing Well"
date_read: "2022-04-21"
created_date: "2022-04-21"
isbn: "9780060891541"
book_authors:
  - "William Zinsser"
rating: 10
book_cover_url: "/assets/book-notes-covers/on-writing-well.jpg"
takeaway: "William Zinsser does an amazing job of inspiring you to write and gives you an array of tools and mental models to choose from while doing it. This book showed me that writing is like programming. Your first pass is to get it working, the second pass is to make it clearer, and the third pass is to make it easier to process. Caveat: I mostly read the non-fiction sections of this book."
purchase_links:
  - store: "Amazon"
    url: "https://amzn.to/3vR7XfI"
  - store: "Harper Collins"
    url: "https://www.harpercollins.com/products/on-writing-well-william-zinsser?variant=32118081159202"
layout: "_book-notes"
---

Simplicity and clarity are the two virtues of good writing. Eagerly trim the fat out of any sentence. Avoid superfluous adjectives, verbs and adverbs.

> All writing is ultimately a question of solving a problem.

Editing is where the work comes together. Writing is where you get your emotions out.

The reader needs to know why they should stick around and finish your piece as quickly as possible. Load the lead with as much of a hook as you can.

> Writing is an intimate action between two people, conducted on paper...

Good writing leaves room for the reader to interpret what's being told to them. Over-explanation in writing is like telling a joke that needs the listener to be walked through it. Value the readers intelligence.

Your interest in the subject that you're writing about is what will carry the piece. No matter how odd or off-the-beaten-path the content is, if you're genuinely curious, then the reader will be, too.

> Never let anything go out into the world that you don't understand.

Writing for yourself is how you find your audience. Don't spend time catering to a group of people. Cater to your interests. Follow what you find the most compelling, and write about that. Edit for clarity, not to appease someone else.

Value the readers intelligence, but don't assume things within a niche topic are general knowledge. Especially if this knowledge was only obtained while researching what you're writing. Follow the arc of your own journey in your writing. Don't forget where you, the writer, started when you began your work.

Analogous imagery is an important tool to use when explaining unfamiliar concepts. Creating images the reader can hold in their minds-eye will help solidify those concepts, and help them stay top of mind.

It's better to over research than under research. Be sure, however, to only give the reader what they need, when they need it. Your job is to synthesize when writing non-fiction.

Write for other humans, no matter their discipline or history. Jargon-free, easy to digest writing is a super power. Don't succumb to Egotism by throwing around fancy words.

Always build a pyramid of knowledge. Start with the minimum required knowledge, and build from there. Each layer adding to the previous.

Avoid cliches wherever possible. Leave them in your drafts if they help convey a feeling, but edit them out quickly to be replaced with something fresh. Cliches diminish your credit as a writer.

Make yourself laugh when writing. Even if it's just for you, it's important to find joy and lightness in everything you do. Otherwise, the work will lose it's meaning, and feel heavier to the reader.

Stay interested in life, and live curiously. Your encounters, adventures and experiences will be the fuel for your writing.

You don't need to be an expert to write about a topic. In fact, your ignorance might give you the edge in writing about a topic. The journey you take can be the story of your work. Your reader is likely picking up your work at a similar starting point to when you began.

Organization and planning your story is paramount to a clear journey through your work. The reader should feel as though each piece fits in it's place, like a puzzle coming together before their eyes.

Revision shouldn’t be done quickly. It should be done with immense care and craft. Be prepared to wrestle with your writing.
