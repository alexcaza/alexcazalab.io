---
book_title: "Spelunky"
date_read: "2022-03-27"
created_date: "2022-03-27"
isbn: "978­-1-­940535­-11-­1"
book_authors:
  - "Derek Yu"
rating: 9
book_cover_url: "/assets/book-notes-covers/spelunky-derek-yu.jpg"
takeaway: "A wonderful first-person recounting of building an indie game. Derek fills the pages with wisdom that spans beyond game design; wisdom that can absolutely be applied to any endeavour where you're going from 0 -> 1."
purchase_links:
  - store: "Amazon"
    url: "https://amzn.to/3wOSFKy"
  - store: "Boss Fight Books"
    url: "https://bossfightbooks.com/products/spelunky-by-derek-yu"
layout: "_book-notes"
---

The creative process is always messy. Moments of clarity are always followed by moments of doubt and panic.

When working in creative fields, experience as much life as you possibly can, and do so mindfully. Inspiration comes from anywhere and everywhere. Be willing to grab hold of it when it presents itself.

The aesthetics drive the rules and dynamics of a game—or product—more than almost any other factor of interactive media/artifacts. Strong aesthetics create boundaries around the experiences you want to create for your player/user.

Look for ideas or solutions that solve many problems, not just their own. When solutions solve for problems greater than what they were created for, the product/game begins to make itself.

Creating safe environments for people to explore and test the limits of their knowledge creates more confident people.

Your level of experience with games changes the way you might assess risks in the environment. I believe this translates to digital products, too. The more comfortable you are navigating the web or digital products due to your history with them, the more likely you are to explore the edges of a product quickly and fearlessly. Less experienced digital product users tend to be more sheepish in their exploration of a product.

When presented with a problem, the obvious answer isn't always the right one. When you can, give the problem a little bit of space before jumping to a solution. The framing of the problem may be partial or incomplete.

A digital world should feel like it continues to live without you, the player, in it. When every part of the world exists for the player, the artificial nature of the experience sticks out.

There's a fine balance between hand-holding and nudging someone to completing their jobs to be done.

When building for achievement, pride or status, the balance becomes making a hard thing easier and keeping it meaningful. Quoted from the book: "The easier a task is, the more people who can carry it out, but the less meaningful it will be."

Superficial rewards for things that a player/user would do naturally might feel less meaningful or reduce the joy of the action. When a superficial reward is unexpected, it might be met with more excitement.

Once you've selected a target audience, create for them, and only them, until you've exhausted how much innovation you can bring to that segment. Consider expanding or creating something new only once you hit that wall.

Balancing difficulty is much more than choosing a linear, exponential or logarithmic function. Difficulty should ebb and flow, taking into account the many contexts at play; the players emotional state, energy levels, curiosity and confusion all affect what is and isn't difficult.

When creating anything, you need to have fun with it yourself. Hide things for you and your team to find, leave easter eggs and be silly. If we don't make room for lightness, we're not fully engaged with what we're creating.
