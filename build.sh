#!/bin/bash

# Ensure public folder is there before build happens
mkdir -p resources/public

# Run main function and build static site
lein run

# Copy files into main public directory
cp -r resources/public public

# Fetch sitemap CLI
wget -qO- https://github.com/graingiant/static-site-sitemap-generator/releases/download/v0.0.2/generate-sitemap-v0.0.2.tar.gz | tar zx
sh generate-sitemap --path=public --url="https://alexcaza.com/"

# Cleanup
rm -rf generate-sitemap generate-sitemap-v0.0.1.tar.gz